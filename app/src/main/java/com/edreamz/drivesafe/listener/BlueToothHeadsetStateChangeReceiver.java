package com.edreamz.drivesafe.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.edreamz.drivesafe.utility.Constants;

public class BlueToothHeadsetStateChangeReceiver extends BroadcastReceiver {
	private static final String TAG = com.edreamz.drivesafe.listener.BlueToothHeadsetStateChangeReceiver.class
			.getName();
	private static int audioState = 129;
	private static int headsetState = 129;
	
	public static int getHeadsetAudioState() {
		return audioState;
	}

	public static int getHeadsetState() {
		return headsetState;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent != null) {
			if (intent.getAction().equalsIgnoreCase(
					Constants.ACTION_HEADSET_STATE_CHANGED)) {
				com.edreamz.drivesafe.listener.BlueToothHeadsetStateChangeReceiver.headsetState = intent
						.getIntExtra(Constants.EXTRA_STATE, Constants.EOF);
				Log.i(TAG, "head set :" + headsetState);

				Toast.makeText(context,"head set :" + headsetState,1000).show();


			} else if (intent.getAction().equalsIgnoreCase(
					Constants.ACTION_AUDIO_STATE_CHANGED)) {
				com.edreamz.drivesafe.listener.BlueToothHeadsetStateChangeReceiver.audioState = intent
						.getIntExtra(Constants.EXTRA_AUDIO_STATE, Constants.EOF);
				Log.i(TAG, "audio state :" + audioState);

				Toast.makeText(context,"audio state :" + audioState,1000).show();
			}

		}
	}

}
