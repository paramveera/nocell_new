package com.edreamz.drivesafe.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edreamz.drivesafe.R;
import com.edreamz.drivesafe.services.MainService;

public class LauncherScreen extends Activity implements OnClickListener {
    private static final String TAG = com.edreamz.drivesafe.ui.LauncherScreen.class.getName();
    private Button startServiceBtn;
    private String headerStr = "";
    private String msgStr = "";
    private String btnStr = "";
    private ImageView actionbar_moreIV = null;
    private static com.edreamz.drivesafe.ui.LauncherScreen launcherScreen= null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        registerReceiver(uiUpdated, new IntentFilter("LOCATION_SPEED_UPDATED"));
        launcherScreen = this;
		setContentView(R.layout.main_view);
		startServiceBtn = (Button) findViewById(R.id.drivingstatusview);
		startServiceBtn.setOnClickListener(this);
        actionbar_moreIV = (ImageView) findViewById(R.id.actionbar_btn);
        actionbar_moreIV.setOnClickListener(this);
        invalidateUI(MainService.getInstance() != null);

	}
    private BroadcastReceiver uiUpdated= new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            try {

                if(intent!=null) {
                    ((TextView) findViewById(R.id.tspeedlimit)).setText("Speed :" + intent.getExtras().getString("speed"));

                }

        }
            catch (Exception e)
            {
            }
        }
    };

    @Override
    protected void onDestroy() {

        unregisterReceiver(uiUpdated);

    }
	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View v) {
        if(v == startServiceBtn) {


            LocationManager manager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                //Ask the user to enable GPS
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Location Manager");
                builder.setMessage("Would you like to enable GPS?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Launch settings, allowing user to make a change
                        Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(i);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //No location service, no Activity
                        finish();
                    }
                });
                builder.create().show();
            }


            if (MainService.getInstance() == null) {
                Intent serviceStartIntent = new Intent(this,
                        MainService.class);
                serviceStartIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startService(serviceStartIntent);
                Toast.makeText(this, getResources().getString(R.string.toast_service_start), Toast.LENGTH_SHORT).show();
                invalidateUI(true);
            } else {
                MainService.getInstance().stopSelf();
                Toast.makeText(this, getResources().getString(R.string.toast_service_stop), Toast.LENGTH_SHORT).show();
                invalidateUI(false);
            }
        }else if(v == actionbar_moreIV){
            showMenuDrawer(v);
        }
	}

    private void showMenuDrawer(View v){
        int topMargin = ((((View) v.getParent()).getTop() + ((View) v
                .getParent()).getHeight())) - (v.getTop() + v.getHeight());
        CustomMenuDrawer.showMenuDrawer(this, v, 0, topMargin);
    }

    public void invalidateUI(boolean isServiceRunning){
        if(!isServiceRunning) {
            headerStr = getResources().getString(R.string.lbl_serivce_stop_header);
            msgStr = getResources().getString(R.string.lbl_serivce_stop_msg);
            btnStr = getResources().getString(R.string.lbl_service_start);
        }else{
            headerStr = getResources().getString(R.string.lbl_serivce_start_header);
            msgStr = getResources().getString(R.string.lbl_serivce_start_msg);
            btnStr = getResources().getString(R.string.lbl_service_stop);
        }
      //  ((TextView)findViewById(R.id.textView1)).setText(headerStr);
       // ((TextView)findViewById(R.id.textView2)).setText(msgStr);
        startServiceBtn.setText(btnStr);
    }

    public static LauncherScreen getInstance(){
        return launcherScreen;
    }
    public void updateSpeed(String speed){
        try {
            Toast.makeText(getApplicationContext(), "Update the UI Speed"+speed,
                    100).show();

         //   String title = String.format("Speed Limit : ", speed);

        }catch(Exception ex){
            Log.w(TAG, "Exception :"+ex);
        }
    }


}
